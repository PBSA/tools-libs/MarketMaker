-e git+https://github.com/pbsa/python-peerplays.git@develop#egg=peerplays

Flask
Flask-Assets
Flask-Mail
Flask-Markdown
Flask-WTF
Jinja2
Markdown
PyYAML
requests
webassets>=0-12.1
flask-script
flask-sqlalchemy
mysqlclient==1.3.7
SQLAlchemy==1.2.18
uwsgi
bookiesports
ecdsa>=0.14 # not directly required, pinned by Snyk to avoid a vulnerability